const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const { response } = require('express');
const { errorMonitor } = require('stream');
const app = express();
const db = require('./db.js');
const { student, aktivnost, tip } = require('./db.js');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname));



app.get("/", function (req, res) {
    res.sendFile(__dirname + "/raspored.html");
});
/*
app.post("/predmet", function(req,res) {
let tijelo = req.body;
console.log(tijelo);
let novalinija=tijelo.predmet+"\n";
fs.appendFile('predmeti.txt', novalinija, function(err){
if(err) throw err;
res.json({message:"Uspjesno dodan predmet", data:novalinija});

});
});

app.post("/aktivnosti", function(req, res) {

let tijelo = req.body;
let novaLinija=tijelo.predmet+","+tijelo.aktivnost+","+tijelo.pocetak+","+tijelo.kraj+","+tijelo.dan+"\n";
fs.appendFile('aktivnosti.txt', novaLinija, function(err){
if(err)throw err;
res.json({message:"Uspješno dodana aktivnost", data:novaLinija})
});
});
        


app.get("/predmet/:naziv/aktivnost/", function(req,res) {
fs.readFile('aktivnosti.txt', (err,contents)=>{
if(err){
    response.writeHead(504,{'Content.Type':'application/json'});
throw err;
} 

let podaci = contents.toString('utf-8').split("\n");
let niz = [];
let tijelo=req.params['naziv'];
for(let i=0;i<podaci.length-1;++i) {
    let pojedinacni = podaci[i].split(",");
   if(pojedinacni[0]==tijelo){
    let objekat = { nazivPredmeta : pojedinacni[0], tip : pojedinacni[1], vrijemePocetka : pojedinacni[2], vrijemeKraja : pojedinacni[3], dan : pojedinacni[4] };
    niz.push(objekat);
    }
}
res.send(niz);
});
});

*/

app.get("/v2/predmet", function(req, res) {

    let nizPredmeta = [];
    db.predmet.findAll().then(function (users) {
        users.forEach(element => {
            nizPredmeta.push(element.get('naziv'));
        });
        res.send(nizPredmeta);
    });

});


app.get("/v2/aktivnost", function(req, res) {

    let nizAktivnosti = [];
    db.aktivnost.findAll().then(function (users) {
        users.forEach(element => {
            nizAktivnosti.push(element.get('naziv'), element.get('pocetak'), element.get('kraj'));
        });
        res.send(nizAktivnosti);
    });

});

app.get("/v2/dan", function(req, res) {

    let nizDana = [];
    db.dan.findAll().then(function (users) {
        users.forEach(element => {
            nizDana.push(element.get('naziv'));
        });
        res.send(nizDana);
    });

});

app.get("/v2/grupa", function(req, res) {

    let nizGrupa = [];
    db.grupa.findAll().then(function (users) {
        users.forEach(element => {
            nizGrupa.push(element.get('naziv'));
        });
        res.send(nizGrupa);
    });
});

app.get("/v2/student", function(req, res) {

    let nizStudenata = [];
    db.student.findAll().then(function (users) {
        users.forEach(element => {
            nizStudenata.push(element.get('ime'), element.get('index'));
        });
        res.send(nizStudenata);
    });
});

app.get("/v2/tip", function(req, res) {

    let nizTipova = [];
    db.tip.findAll().then(function (users) {
        users.forEach(element => {
            nizTipova.push(element.get('naziv'));
        });
        res.send(nizTipova);
    });

});

app.post("/v2/student" , function(req,res) {
    var tijelo=req.body;
  console.log(tijelo.length);
let niz=[];
let nizImena=[];

for(let i=0;i<tijelo.length;i++) {
    niz.push(tijelo[i].index);
    nizImena.push(tijelo[i].ime);
}
console.log(niz);

db.student.findAll({
    where : {
        index : niz 
    }, include : { model: db.grupa, as: "grupe",through: {
        attributes: [],
      } }
}).then(users=>{
if(users.length>0) {
}
let nizVracanja=[];
for(let i=0;i<niz.length;i++) {
    let usr=users.find(user=>user.index==niz[i]);
    let usr2=users.find(user=>user.ime==nizImena[i] && user.index==niz[i]);
    if(usr2) {
        db.grupa.findOne({where : {
        naziv: tijelo[0].grupa
        }}).then(rezultat=> {
           let group=usr2.dataValues.grupe.find(user=>user.dataValues.naziv==tijelo[0].grupa);
let predmet=usr2.dataValues.grupe.find(user=>user.dataValues.PredmetId==rezultat.dataValues.PredmetId);
let pomocna =false;
for(let z=0;z<usr2.dataValues.grupe.length;z++) {
    if(usr2.dataValues.grupe[z].dataValues.PredmetId==rezultat.dataValues.PredmetId){
pomocna=true;
}}
let niz= [];
 if(group) {

 } 
 else if(predmet && !group && pomocna) {
     for(let j=0;j<usr2.dataValues.grupe.length;j++) {
    if(usr2.dataValues.grupe[j]==predmet)niz.push(rezultat);
    else niz.push(usr2.dataValues.grupe[j]);
}
     usr2.setGrupe(niz);
 }
 else  {
     for(let j=0;j<usr2.dataValues.grupe.length;j++) {
        niz.push(usr2.dataValues.grupe[j]);     
     }

    niz.push(rezultat);
   usr2.setGrupe(niz);
 
}
    });
}
    
   else if(usr && !usr2) {nizVracanja.push("Student "+ tijelo[i].ime+" nije kreiran jer postoji student "+usr.ime+" sa istim indeksom "+usr.index);}
    
    else {


        db.student.create({
            ime: tijelo[i].ime,
            index:tijelo[i].index
         }).then(rezultat => {
db.grupa.findOne({where  : {
         naziv : tijelo[0].grupa
}}).then(result=>{
    if(result)
    rezultat.setGrupe(result);
});

         });  
           
        }    
}

res.statusCode=200;
res.send(nizVracanja);

});  

});




app.post("/v2/aktivnost" , function(req,res) {
    db.aktivnost.create({
       naziv: req.body.aktivnost,
       pocetak : req.body.pocetak,
           kraj : req.body.kraj
    }).then(rezultat =>{
db.predmet.findOne({where: {
            naziv : req.body.predmet
        }}).then(p=> {
            if(p)   
     p.setAktivnostPredmeta([rezultat]);
    });
    db.dan.findOne({where : {
        naziv: req.body.dan
    }}).then(d=> {
        if(d)
        d.setAktivnostDana([rezultat]);
    });
        db.tip.findOne({where : {
    naziv : req.body.aktivnost
}}).then(t=> {
if(t)
t.setAktivnostTipa([rezultat]);
});
    });    
    console.log("zavriso");
    });
    app.post("/v2/dan" , function(req,res) {
        db.dan.create({
           naziv: req.body.naziv
        }).then(naziv => res.send(naziv));
        });
        app.post("/v2/grupa" , function(req,res) {
            db.grupa.create({
               naziv: req.body.naziv,
               PredmetId:req.body.PredmetId
            }).then(naziv => res.send(naziv));
            });
            app.post("/v2/predmet" , function(req,res) {
                db.predmet.create({
                   naziv: req.body.predmet
                }).then(naziv => {

                    db.grupa.findOne({where: {
                        naziv:req.body.grupa
                    }}).then(grupe=> {
                        if(grupe)
                        naziv.addGrupaPredmeta(grupe);
                    });
                    
                });
                });
                app.post("/v2/tip" , function(req,res) {
                    db.tip.create({
                       naziv: req.body.naziv
                    }).then(naziv => res.send(naziv));
                    });



                    app.put('/v2/student/:id', function (req, res ) {
                       
                        let tijelo=req.params['id'];
                       let studenti=req.body;
                        db.student.update(
                          {ime: studenti.ime,
                        index:studenti.index},
                          {where:{ id : tijelo}}
                        )
                        .then(function(rowsUpdated) {
                          res.send(rowsUpdated)
                        })
                       });

                       app.put('/v2/predmet/:id', function (req, res ) {
                       
                        let tijelo=req.params['id'];
                       let predmeti=req.body;
                        db.predmet.update(
                          {naziv: predmeti.ime},
                          {where:{ id : tijelo}}
                        )
                        .then(function(rowsUpdated) {
                          res.send(rowsUpdated)
                        })
                       });

                       app.put('/v2/tip/:id', function (req, res ) {
                       
                        let tijelo=req.params['id'];
                       let tipovi=req.body;
                        db.tip.update(
                          {ime: tipovi.naziv},
                          {where:{ id : tijelo}}
                        )
                        .then(function(rowsUpdated) {
                          res.send(rowsUpdated)
                        })
                       });

                       app.put('/v2/dani/:id', function (req, res ) {
                       
                        let tijelo=req.params['id'];
                       let dani=req.body;
                        db.dan.update(
                          {naziv: dani.naziv},
                          {where:{ id : tijelo}}
                        )
                        .then(function(rowsUpdated) {
                          res.send(rowsUpdated)
                        })
                       });
                       app.put('/v2/aktivnosti/:id', function (req, res ) {
                       
                        let tijelo=req.params['id'];
                       let aktivnosti=req.body;
                        db.aktivnost.update(
                          {naziv: aktivnosti.ime,
                        pocetak:aktivnosti.pocetak,
                        kraj:aktivnosti.kraj,
                        PredmetId:aktivnosti.PredmetId,
                        DanId:aktivnosti.DanId,
                        TipId:aktivnosti.TipId
                    },
                          {where:{ id : tijelo}}
                        )
                        .then(function(rowsUpdated) {
                          res.send(rowsUpdated)
                        })
                       });
                       app.put('/v2/grupa/:id', function (req, res ) {
                       
                        let tijelo=req.params['id'];
                       let grupe=req.body;
                        db.grupa.update(
                          {naziv: grupe.naziv,
                        PredmetId:grupe.PredmetId},
                          {where:{ id : tijelo}}
                        )
                        .then(function(rowsUpdated) {
                          res.send(rowsUpdated)
                        })
                       }); 

  
                       app.delete('/v2/student/:id', function(req, res) {
                        var id = req.params['id'];
                        student.destroy(
                            {where: {id:id}},
                            {truncate: true}
                        ).then(() => {
                            res.send("{}"); 
                        });
                    });
                    app.delete('/v2/aktivnost/:id', function(req, res) {
                        var id = req.params['id'];
                        aktivnost.destroy(
                            {where: {id:id}},
                            {truncate: true}
                        ).then(() => {
                            res.send("{}"); 
                        });
                    });
         
                    app.delete('/v2/dan/:id', function(req, res) {
                        var id = req.params['id'];
                        dan.destroy(
                            {where: {id:id}},
                            {truncate: true}
                        ).then(() => {
                            res.send("{}"); 
                        });
                    });
                    app.delete('/v2/grupa/:id', function(req, res) {
                        var id = req.params['id'];
                        grupa.destroy(
                            {where: {id:id}},
                            {truncate: true}
                        ).then(() => {
                            res.send("{}"); 
                        });
                    });
                    app.delete('/v2/predmet/:id', function(req, res) {
                        var id = req.params['id'];
                        predmet.destroy(
                            {where: {id:id}},
                            {truncate: true}
                        ).then(() => {
                            res.send("{}"); 
                        });
                    });
                    app.delete('/v2/tip/:id', function(req, res) {
                        var id = req.params['id'];
                        tip.destroy(
                            {where: {id:id}},
                            {truncate: true}
                        ).then(() => {
                            res.send("{}"); 
                        });
                    });


/*
app.get("/aktivnosti", function(req,res) {
console.log("aktivity");
    fs.readFile('aktivnosti.txt', (err, contents) => {  
        if (err) {
            response.writeHead(504, {'Content-Type': 'application/json'});
            throw err;
        }
        let podaci = contents.toString('utf-8').split("\n");
        let niz = [];
        for(let i=0; i < podaci.length-1; ++i){
            let pojedinacni = podaci[i].split(",");
            let objekat = { nazivPredmeta : pojedinacni[0], tip : pojedinacni[1], vrijemePocetka : pojedinacni[2], vrijemeKraja : pojedinacni[3], dan : pojedinacni[4] };
            niz.push(objekat);
        }
        res.send(niz);
    });
});

app.delete("/all",function(req,res){

    fs.unlink('predmeti.txt',function(err, data){
    });
    fs.unlink('aktivnosti.txt', function(err, data){ 
    });
    
    let Filedata='';
    fs.writeFile('predmeti.txt', Filedata, function(err) {
    if(err)console.log(err);
    });
    fs.writeFile('aktivnosti.txt', Filedata, function(err){
    if(err)console.log(err);
    });

});

app.delete('/aktivnost/:naziv', function(req,res){
    const removeLines = (data, lines = []) => {
        return data.toString()
            .split('\n')
            .filter((val, idx) => lines.indexOf(idx) === -1)
            .join('\n');
    }

    fs.readFile('aktivnosti.txt',function(err, constext){
if(err)throw err;
console.log("predmettttt");
let podaci = constext.toString('utf-8').split("\n");
let niz = [];
let tijelo = req.params['naziv'];
for(let i=0;i< podaci.length;i++) {
    var pojedinacni=podaci[i].split(",");
    if(pojedinacni[0]==tijelo) niz.push(i); 
}
fs.writeFile('aktivnosti.txt', removeLines(constext, niz),'utf8', function(err){
if(err)throw err;
console.log("Aktivnost je uspjesno obrisana");
});

});


});
app.delete('/predmet/:naziv', function(req,res){
    const removeLines = (data, lines = []) => {
        return data.toString()
            .split('\n')
            .filter((val, idx) => lines.indexOf(idx) === -1)
            .join('\n');
    }
fs.readFile('predmeti.txt', function(err, constext) {
    if(err)throw err;
    let podaci=constext.toString('utf8').split('\n');
    let niz = [];
    let tijelo =req.params['naziv'];
    for(let i=0;i<podaci.length;i++) {
        if(podaci[i]==tijelo)niz.push(i);
    }
fs.writeFile('predmeti.txt', removeLines(constext, niz), 'utf-8', function(err){
if(err)throw err;
console.log("Uspjesno obrisan predmet");
});
});
}); 
*/

const PORT = 3000;


db.sequelize.sync({ force: true }).then(function () {
app.listen(PORT, ()=>console.log('Server started..'));
});

module.exports = app;