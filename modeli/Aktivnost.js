const Sequelize=require("sequelize");

module.exports=function(sequelize, data){
  const Aktivnost=sequelize.define("Aktivnost",{
   naziv:Sequelize.STRING,
   pocetak:Sequelize.TIME,
   kraj:Sequelize.TIME
  })
  return Aktivnost;  
};