const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2018285","root","root",{host:"127.0.0.1",dialect:"mysql",logging:false});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;



//import modela
db.aktivnost=sequelize.import(__dirname+'/modeli/Aktivnost.js');
db.dan=sequelize.import(__dirname+'/modeli/Dan.js');
db.grupa=sequelize.import(__dirname+'/modeli/Grupa.js');
db.predmet=sequelize.import(__dirname+'/modeli/Predmet.js');
db.tip=sequelize.import(__dirname+'/modeli/Tip.js');
db.student=sequelize.import(__dirname+'/modeli/Student.js');

//Predmet 1-N Grupa
db.predmet.hasMany(db.grupa, {as:'grupaPredmeta'});

//Aktivnost N-1 Predmet
db.predmet.hasMany(db.aktivnost,{as:'aktivnostPredmeta'});

//Aktivnost N-0 Grupa
db.grupa.hasMany(db.aktivnost,{as:'aktivnostGrupe'} , {
    allowNull: false
  });
//Aktivnost N-1 Dan
db.dan.hasMany(db.aktivnost,{as:'aktivnostDana'});

//Aktivnost N-1 Tip
db.tip.hasMany(db.aktivnost,{as:'aktivnostTipa'});


//Student N-M Grupa

db.studentGrupa=db.grupa.belongsToMany(db.student,{as:'studenti', through:'studenti_grupe', foreignKey:'GrupaId'});
db.student.belongsToMany(db.grupa,{as:'grupe',through:'studenti_grupe', foreignKey:'StudentId'});

module.exports=db;